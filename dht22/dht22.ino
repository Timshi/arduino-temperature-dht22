#include <DHT.h>

DHT dht(D1, DHT22);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  float temperature = dht.readTemperature();
  float fahrenheit = dht.readTemperature(true);
  float humidity = dht.readHumidity();

  Serial.println(getTemperatureString(temperature));
  Serial.println(getFahrenheitString(fahrenheit));
  Serial.println(getHumidityString(humidity));
  Serial.println("==========================================");

  delay(2000); // read new values only every 2 seconds because the sensor is slow
}

String getTemperatureString(float temperature) {
  return String(temperature) + " °C";
}

String getFahrenheitString(float fahrenheit) {
  return String(fahrenheit) + " °F";
}

String getHumidityString(float humidity) {
  return String(humidity) + " %";
}